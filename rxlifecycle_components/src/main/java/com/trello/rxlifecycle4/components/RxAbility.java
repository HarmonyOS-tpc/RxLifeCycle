/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trello.rxlifecycle4.components;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.BehaviorSubject;

import com.trello.rxlifecycle4.harmony.AbilityEvent;
import com.trello.rxlifecycle4.harmony.RxLifecycleHarmony;
import com.trello.rxlifecycle4.LifecycleProvider;
import com.trello.rxlifecycle4.LifecycleTransformer;
import com.trello.rxlifecycle4.RxLifecycle;
import com.trello.rxlifecycle4.rxlifecycle_annotation.CallSuper;
import com.trello.rxlifecycle4.rxlifecycle_annotation.CheckResult;
import com.trello.rxlifecycle4.rxlifecycle_annotation.NonNull;

/**
 * This class provide the RxLifecycle feature to Ability
 */
public abstract class RxAbility extends Ability implements LifecycleProvider<AbilityEvent> {
    private final BehaviorSubject<AbilityEvent> lifecycleSubject = BehaviorSubject.create();

    @Override
    @NonNull
    @CheckResult
    public final Observable<AbilityEvent> lifecycle() {
        return lifecycleSubject.hide();
    }

    @Override
    @NonNull
    @CheckResult
    public final <T> LifecycleTransformer<T> bindUntilEvent(@NonNull AbilityEvent event) {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, event);
    }

    @Override
    @NonNull
    @CheckResult
    public final <T> LifecycleTransformer<T> bindToLifecycle() {
        return RxLifecycleHarmony.bindAbility(lifecycleSubject);
    }

    @Override
    @CallSuper
    protected void onStart(Intent intent) {
        super.onStart(intent);
        lifecycleSubject.onNext(AbilityEvent.START);
    }

    @Override
    @CallSuper
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        lifecycleSubject.onNext(AbilityEvent.FOREGROUND);
    }

    @Override
    @CallSuper
    protected void onActive() {
        super.onActive();
        lifecycleSubject.onNext(AbilityEvent.ACTIVE);
    }

    @Override
    @CallSuper
    protected void onInactive() {
        lifecycleSubject.onNext(AbilityEvent.INACTIVE);
        super.onInactive();
    }

    @Override
    @CallSuper
    protected void onBackground() {
        lifecycleSubject.onNext(AbilityEvent.BACKGROUND);
        super.onBackground();
    }

    @Override
    @CallSuper
    protected void onStop() {
        lifecycleSubject.onNext(AbilityEvent.STOP);
        super.onStop();
    }
}
