/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trello.rxlifecycle4.sample;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import com.trello.rxlifecycle4.harmony.FractionEvent;
import com.trello.rxlifecycle4.components.RxFraction;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

import static com.trello.rxlifecycle4.sample.Constants.UNSUBSCRIBE_STRING_ON_ATTACH;
import static com.trello.rxlifecycle4.sample.Constants.UNSUBSCRIBE_STRING_ON_START;
import static com.trello.rxlifecycle4.sample.Constants.UNSUBSCRIBE_STRING_ON_ACTIVE;
import static com.trello.rxlifecycle4.sample.Constants.PERIOD_100;

/**
 * This is TestFraction
 */
public class TestFraction extends RxFraction {
    private static final String TAG = TestFraction.class.getName();


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container,
                                            Intent intent) {
        final Component component = super.onComponentAttached(scatter, container, intent);
        LogUtil.info(TAG, "onComponentAttached");

        /* --- Test case 7 (RxLifecycle_fraction 7. --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_7", UNSUBSCRIBE_STRING_ON_ATTACH))
                .compose(this.<Long>bindUntilEvent(FractionEvent.START))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_7",
                        "Started in onComponentAttached(), running until in onStart(): " + num));

        /* --- Test case 8 (RxLifecycle_fraction 8) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_8", UNSUBSCRIBE_STRING_ON_ATTACH))
                .compose(this.<Long>bindUntilEvent(FractionEvent.ACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_8",
                        "Started in onComponentAttached(), running until in onActive(): " + num));

        /* --- Test case 9 (RxLifecycle_fraction_9) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_9", UNSUBSCRIBE_STRING_ON_ATTACH))
                .compose(this.<Long>bindUntilEvent(FractionEvent.INACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_9",
                        "Started in onComponentAttached(), running until in onInactive(): " + num));

        /* --- Test case 10 (RxLifecycle_fraction 10) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_10", UNSUBSCRIBE_STRING_ON_ATTACH))
                .compose(this.<Long>bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_10",
                        "Started in onComponentAttached(), running until in onComponentDetach(): " + num));

        /* --- Test case 11 (RxLifecycle_fraction 11) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_11", UNSUBSCRIBE_STRING_ON_ATTACH))
                .compose(this.<Long>bindUntilEvent(FractionEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_11",
                        "Started in onComponentAttached(), running until in onStop(): " + num));

        /* --- Test case 12 (RxLifecycle_fraction_12) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_12", UNSUBSCRIBE_STRING_ON_ATTACH))
                .compose(this.<Long>bindUntilEvent(FractionEvent.DETACH))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_12",
                        "Started in onComponentAttached(), running until in onComponentDetach(): " + num));
        return component;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        LogUtil.info(TAG, "onStart");

        /* --- Test case 1 (RxLifecycle_fraction 1)--- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_1", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_1",
                        "Started in onStart(), running until in onStop(): " + num));

        /* --- Test case 3 (RxLifecycle_fraction_3) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_3", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(FractionEvent.ACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_3",
                        "Started in onStart(), running until in onActive(): " + num));

        /* --- Test case 4 (RxLifecycle_fraction_4) --- */
        Observable.interval(PERIOD_100, TimeUnit.MILLISECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_4", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(FractionEvent.INACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_4",
                        "Started in onStart(), running until in onInactive(): " + num));

        /* --- Test case 13 (RxLifecycle_fraction_13) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_13", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(FractionEvent.BACKGROUND))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_13",
                        "Started in onStart(), running until in onBackground(): " + num));

        /* --- Test case 14 (RxLifecycle_fraction_14) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_14", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(FractionEvent.DETACH))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_14",
                        "Started in onStart(), running until in onComponentDetach(): " + num));
    }

    @Override
    protected void onActive() {
        super.onActive();
        LogUtil.info(TAG, "onActive");

        /* --------- Test case 2 (RxLifecycle_fraction 2) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_2", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_2",
                        "Started in onActive(), running until in onInactive(): " + num));

        /* -------------- Test case 5 (RxLifecycle_fraction_5)------ */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_5", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindUntilEvent(FractionEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_5",
                        "Started in onActive(), running until in onStop(): " + num));

        /* ------------ Test case 16 (RxLifecycle_fraction_16) ---- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_16", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindUntilEvent(FractionEvent.DETACH))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_16",
                        "Started in onActive(), running until in onComponentDetach(): " + num));

        /* ------------ Test case 15 (RxLifecycle_fraction_15) ---- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_15", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindUntilEvent(FractionEvent.BACKGROUND))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_15",
                        "Started in onActive(), running until onBackground(): " + num));
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        LogUtil.info(TAG, "onInactive");

        /* ----- Test case 6 (RxLifecycle_fraction_6)------ */
        Observable.interval(PERIOD_100, TimeUnit.MILLISECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_6",
                        "Unsubscribing subscription from onInactive()"))
                .compose(this.<Long>bindUntilEvent(FractionEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_6",
                        "Started in onInactive(), running until in onStop(): " + num));

        /* ------ Test case 17 (RxLifecycle_fraction_17) ------------- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_17",
                        "Unsubscribing subscription from onBackground()"))
                .compose(this.<Long>bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_17",
                        "Started in onInactive(), running until in onBackground(): " + num));

        /* --- Test case 18 (RxLifecycle_fraction_18) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_18",
                        "Unsubscribing subscription from onInactive()"))
                .compose(this.<Long>bindUntilEvent(FractionEvent.DETACH))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_18",
                        "Started in onInactive(), running until in onComponentDetach(): " + num));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtil.info(TAG, "onStop");

        /* ---------- Test case 21 (RxLifecycle_fraction_21) */
        Observable.interval(PERIOD_100, TimeUnit.MILLISECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_21",
                        "Unsubscribing subscription from onStop()"))
                .compose(this.<Long>bindUntilEvent(FractionEvent.DETACH))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_21",
                        "Started in onStop(), running until in onComponentDetach(): " + num));
    }

    @Override
    protected void onComponentDetach() {
        super.onComponentDetach();
    }


    @Override
    protected void onBackground() {
        super.onBackground();
        /* --- Test case 19 (RxLifecycle_fraction_19) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_19",
                        "Unsubscribing subscription from onBackground()"))
                .compose(this.<Long>bindUntilEvent(FractionEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_19",
                        "Started in onBackground(), running until onStop(): " + num));

        /* --- Test case 20 (RxLifecycle_fraction_20) --- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_20",
                        "Unsubscribing subscription from onBackground()"))
                .compose(this.<Long>bindUntilEvent(FractionEvent.DETACH))
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_20",
                        "Started in onBackground(), running until onComponentDetach(): " + num));
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);

        /* --- Test case 22 (RxLifecycle_fraction 22)--- */
        Observable.interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_fraction_22",
                        "Unsubscribing subscription from onForeground()"))
                .compose(this.<Long>bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_fraction_22",
                        "Started in onForeground(), running until onBackground(): " + num));
    }
}
