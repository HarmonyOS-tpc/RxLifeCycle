/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trello.rxlifecycle4.sample.slice;

import ohos.aafwk.content.Intent;

import com.trello.rxlifecycle4.harmony.AbilitySliceEvent;
import com.trello.rxlifecycle4.components.RxAbilitySlice;
import com.trello.rxlifecycle4.sample.LogUtil;
import com.trello.rxlifecycle4.sample.ResourceTable;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

import static com.trello.rxlifecycle4.sample.Constants.PERIOD;
import static com.trello.rxlifecycle4.sample.Constants.UNSUBSCRIBE_STRING_ON_ACTIVE;
import static com.trello.rxlifecycle4.sample.Constants.UNSUBSCRIBE_STRING_ON_START;

/**
 * This is Test Ability Slice class which
 */
public class TestAbilitySlice extends RxAbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_test);

        /* --- Testcase 1 ( RxLifecycle_ability_slice_1 ) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_slice_1", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_slice_1",
                        "Started in onStart(), running until in onStop(): " + num));

        /* --- Testcase 3 (RxLifecycle_ability_slice_3) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_slice_3", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(AbilitySliceEvent.ACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_slice_3",
                        "Started in onStart(), running until in onActive(): " + num));

        /* --- Testcase 4 (RxLifecycle_ability_4) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_slice_4", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(AbilitySliceEvent.INACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_slice_4",
                        "Started in onStart(), running until in onInactive(): " + num));

        /* --- Testcase 5 (RxLifecycle_ability_slice_5) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_slice_5", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(AbilitySliceEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_slice_5",
                        "Started in onStart(), running until in onStop(): " + num));
    }

    @Override
    public void onActive() {
        super.onActive();
        /* --- Testcase 2 (RxLifecycle_ability_slice_2) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_slice_2", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_slice_2",
                        "Started in onActive(), running until in onInactive(): " + num));

        /* --- Testcase 6 (RxLifecycle_ability_slice_6) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_slice_6", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindUntilEvent(AbilitySliceEvent.INACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_slice_6",
                        "Started in onActive(), running until in onInactive(): " + num));

        /* --- Testcase 7 (RxLifecycle_ability_slice_7) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_slice_7", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindUntilEvent(AbilitySliceEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_slice_7",
                        "Started in onActive(), running until in onStop(): " + num));
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        /* --- Testcase 8 (RxLifecycle_ability_slice_8) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_slice_8",
                        "Unsubscribing subscription from onInactive()"))
                .compose(this.<Long>bindUntilEvent(AbilitySliceEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_slice_8",
                        "Started in onInactive(), running until in onStop(): " + num));
    }

}
