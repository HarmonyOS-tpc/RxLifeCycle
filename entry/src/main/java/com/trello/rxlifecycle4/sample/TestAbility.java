/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trello.rxlifecycle4.sample;

import ohos.aafwk.content.Intent;

import com.trello.rxlifecycle4.harmony.AbilityEvent;
import com.trello.rxlifecycle4.components.RxAbility;
import com.trello.rxlifecycle4.sample.slice.TestAbilitySlice;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

import static com.trello.rxlifecycle4.sample.Constants.PERIOD;
import static com.trello.rxlifecycle4.sample.Constants.UNSUBSCRIBE_STRING_ON_ACTIVE;
import static com.trello.rxlifecycle4.sample.Constants.UNSUBSCRIBE_STRING_ON_START;


/**
 * This is TestAbility
 */
public class TestAbility extends RxAbility {
    private static final String TAG_LOG = TestAbility.class.getName();
    private boolean isDisplayAbilityLog = false;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        LogUtil.info(TAG_LOG, "onStart()");
        isDisplayAbilityLog = intent.getBooleanParam(Constants.KEY_ABILITY, false);

        if (!isDisplayAbilityLog) {
            super.setMainRoute(TestAbilitySlice.class.getName());
            return;
        } else {
            setUIContent(ResourceTable.Layout_simple_fraction_layout);
        }

        /* --- Testcase 1 (RxLifecycle_ability_1) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_1", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_1",
                        "Started in onStart(), running until in onStop(): " + num));

        /* --- Testcase 3 (RxLifecycle_ability_3) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_3", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(AbilityEvent.ACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_3",
                        "Started in onStart(), running until in onActive(): " + num));

        /* --- Testcase 4 (RxLifecycle_ability_4) ------------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_4", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(AbilityEvent.INACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_4",
                        "Started in onStart(), running until in onInactive(): " + num));

        /* --- Testcase 5 (RxLifecycle_ability_5) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_5", UNSUBSCRIBE_STRING_ON_START))
                .compose(this.<Long>bindUntilEvent(AbilityEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_5",
                        "Started in onStart(), running until in onStop(): " + num));
    }

    @Override
    protected void onActive() {
        super.onActive();
        LogUtil.info(TAG_LOG, "onActive()");
        if (!isDisplayAbilityLog) {
            return;
        }
        /* --- Testcase 2 (RxLifecycle_ability_2) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_2", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindToLifecycle())
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_2",
                        "Started in onActive(), running until in onInactive(): " + num));

        /* --- Testcase 6 (RxLifecycle_ability_6) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_6", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindUntilEvent(AbilityEvent.INACTIVE))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_6",
                        "Started in onActive(), running until in onInactive(): " + num));

        /* --- Testcase 7 (RxLifecycle_ability_7) --------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_7", UNSUBSCRIBE_STRING_ON_ACTIVE))
                .compose(this.<Long>bindUntilEvent(AbilityEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_7",
                        "Started in onActive(), running until in onStop(): " + num));
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        LogUtil.info(TAG_LOG, "onInactive()");
        if (!isDisplayAbilityLog) {
            return;
        }

        /* --- Testcase 8 (RxLifecycle_ability_8) ----------------- */
        Observable.interval(PERIOD, TimeUnit.SECONDS)
                .doOnDispose(() -> LogUtil.info("RxLifecycle_ability_8",
                        "Unsubscribing subscription from onInactive()"))
                .compose(this.<Long>bindUntilEvent(AbilityEvent.STOP))
                .subscribe(num -> LogUtil.info("RxLifecycle_ability_8",
                        "Started in onInactive(), running until in onStop(): " + num));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtil.info(TAG_LOG, "onStop()");
    }
}
