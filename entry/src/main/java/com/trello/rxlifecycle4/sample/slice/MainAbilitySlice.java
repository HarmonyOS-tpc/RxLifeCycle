/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trello.rxlifecycle4.sample.slice;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

import com.trello.rxlifecycle4.components.RxAbilitySlice;
import com.trello.rxlifecycle4.sample.Constants;
import com.trello.rxlifecycle4.sample.ResourceTable;
import com.trello.rxlifecycle4.sample.TestAbility;
import com.trello.rxlifecycle4.sample.TestFractionAbility;

/**
 * This is class populates the main ui
 */
public class MainAbilitySlice extends RxAbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        (findComponentById(ResourceTable.Id_ability)).setClickedListener(this::onClick);
        (findComponentById(ResourceTable.Id_ability_slice)).setClickedListener(this::onClick);
        (findComponentById(ResourceTable.Id_fraction)).setClickedListener(this::onClick);
        (findComponentById(ResourceTable.Id_fraction_ability)).setClickedListener(this::onClick);
    }

    private void launchTestAbility(boolean ability) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(TestAbility.class)
                .build();
        intent.setOperation(operation);
        intent.setParam(Constants.KEY_ABILITY, ability);
        startAbility(intent);
    }

    private void launchTestFractionAbility(boolean fraction) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withAbilityName(TestFractionAbility.class)
                .withBundleName(getBundleName())
                .build();
        intent.setOperation(operation);
        intent.setParam(Constants.KEY_FRACTION, fraction);
        startAbility(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_ability:
                launchTestAbility(true);
                break;
            case ResourceTable.Id_ability_slice:
                launchTestAbility(false);
                break;
            case ResourceTable.Id_fraction:
                launchTestFractionAbility(true);
                break;
            case ResourceTable.Id_fraction_ability:
                launchTestFractionAbility(false);
                break;
            default:
                break;
        }
    }
}
